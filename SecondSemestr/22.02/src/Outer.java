public class Outer {
    private String name;

    public static class Nested{
        public void print(){
            System.out.println("Nested class!!");
            Outer outer = new Outer("Fail");
            System.out.println(outer.name);
        }
    }
    public class Inner{
        public String getOuterName(){
            return Outer.this.name;
        }
    }

    public Outer(String name){
        this.name = name;
    }

    public Outer() {
    }

    public Inner getInner(){
        return new Inner();
    }

    public static void main(String[] args) {
        Outer outer = new Outer("This is a Outer class");
        System.out.println(outer.getInner().getOuterName());

        Outer.Inner inner1 = outer.new Inner();
        Outer.Inner inner2 = new Outer("Second outer").new Inner();
        Outer.Inner inner3 = outer.getInner();

        System.out.println(inner1.getOuterName());
        System.out.println(inner2.getOuterName());
        System.out.println(inner3.getOuterName() + "\n");

        Outer.Nested nested = new Outer.Nested();
        nested.print();
    }
}