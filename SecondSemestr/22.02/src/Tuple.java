public class Tuple<T, L> {
    public final T first;
    public final L second;

    public Tuple(T first, L second){
        this.first = first;
        this.second = second;
    }
}
