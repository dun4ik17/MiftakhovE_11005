public class ThreeTuple<T,L,C> extends Tuple {
    public final C third;

    public ThreeTuple(T first, L second, C third){
        super(first, second);
        this.third = third;
    }
}
