public class RangeInterval extends Range {

    public RangeInterval(int start, int end, int arrayLength) {
        super(end);
        if (start < end && end < arrayLength) {
            counter = start;
        } else
            throw new RuntimeException();
    }
}
