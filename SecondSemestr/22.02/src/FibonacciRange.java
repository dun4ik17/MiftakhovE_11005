import java.util.*;

public class FibonacciRange extends Range {
    private int nextCounter = 1;
    private int c = counter;
    private int counterTwo;
    public FibonacciRange(int length){
         super(length);
     }

    @Override
    public Iterator<Integer> iterator() {
        return new Iterator<Integer>() {
            @Override
            public Integer next() {
                c = counter;
                counterTwo++;
                counter = nextCounter;
                nextCounter += c;
                return c;
            }

            @Override
            public boolean hasNext() {
                return counterTwo <= length;
            }
        };
    }
}
