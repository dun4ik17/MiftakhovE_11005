import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        String text = "";
        try(FileInputStream fileInStream = new FileInputStream("src\\example.txt")){
            int i = -1;
            while((i = fileInStream.read()) != -1){
                text += (char) i;
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }

        text += "\n";

        try(FileInputStream fileInStream = new FileInputStream("src\\test.txt")){
            Scanner sc = new Scanner(fileInStream);
            System.out.println(sc.nextLine());
        }catch(Exception e){
            System.out.println(e.getMessage());
        }

        try(FileOutputStream fileOutStream = new FileOutputStream("src\\test.txt", false)){

            byte[] buffer = text.getBytes();
            fileOutStream.write(buffer);
        } catch(IOException e){
            System.out.println(e.getMessage());
        }

        try(FileInputStream fileInStream = new FileInputStream("src\\test.txt")){
            Scanner sc = new Scanner(fileInStream);
            System.out.println(sc.nextLine());
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
