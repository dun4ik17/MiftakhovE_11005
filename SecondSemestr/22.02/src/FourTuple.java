public class FourTuple<T,L,C,M> extends ThreeTuple {
    public final M fourth;

    public FourTuple(T first, L second, C third, M fourth){
        super(first, second, third);
        this.fourth = fourth;
    }
}
