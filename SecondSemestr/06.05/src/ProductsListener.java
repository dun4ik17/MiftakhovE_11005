import java.io.IOException;

public interface ProductsListener {
    public void productsUpdate() throws IOException, ClassNotFoundException;
}
