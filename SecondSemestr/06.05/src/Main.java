import java.awt.image.AreaAveragingScaleFilter;
import java.io.*;
import java.lang.reflect.Field;
import java.util.*;
import java.util.function.Predicate;

public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        //Task 1
        {
            Comparator<String> treeComparator = (str1, str2) -> Integer.compare(str1.length(), str2.length());
            TreeSet<String> set = new TreeSet<>(treeComparator);

            set.add("asd");
            set.add("asssssdd");
            set.add("asssd");
            set.add("s");
            set.add("sass");

            for (String x : set) {
                System.out.println(x);
            }
        }
        //Task2
        {
            int[] arr = new int[]{1, 5, 3 ,7 ,8, 2, 2, 5, 3, 4, 10};
            System.out.println("Было: \n" + Arrays.toString(arr) + "\nСтало: ");
            System.out.println(Arrays.toString(siftArray(arr, x -> x % 2 == 0)));
        }
        //Task3
        {
            Store main = new Store();
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(".\\src\\Storage.txt"));
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(".\\src\\Storage.txt"));

            main.addListener(() -> main.printAll(in, out));
            main.addListener(() -> {
                HashMap<String, Integer> storage = main.getStorage();
                int result = 0;

                for (Integer x: storage.values()){
                    result += x;
                }

                System.out.println("Заполнено ячеек - " + result);
            });
            main.addListener(() -> {
                HashMap<String, Integer> storage = main.getStorage();
                int max = Integer.MIN_VALUE;
                String maxName = "";

                for (Map.Entry<String, Integer> entry: storage.entrySet()) {
                    if(entry.getValue() > max){
                        max = entry.getValue();
                        maxName = entry.getKey();
                    }
                }

                System.out.println(maxName + " - " + max);
            });

            main.processStore(in ,out);

            in.close();
            out.close();
        }
    }
    public static int[] siftArray(int[] array, Predicate<Integer> predicate){
        int[] newArray = new int[array.length];
        int id = 0;
        for (int i = 0; i < array.length; i++) {
            if(predicate.test(array[i])) {
                newArray[id] = array[i];
                id++;
            }
        }
        int[] result = Arrays.copyOf(newArray, id);
        return result;
    }
}
