import java.io.*;
import java.util.*;
public class Store implements Serializable {
    private String product; //Надо сделать в отдельный класс, но времени нема.....
    private int count;

    private ArrayList<ProductsListener> listeners = new ArrayList<>();
    private HashMap<String, Integer> storageMap;

    public boolean addListener(ProductsListener listener){
        if(listeners.contains(listener))
            return false;
        listeners.add(listener);
        return true;
    }

    public Store(String product, int count){
        this.product = product;
        this.count = count;
    }
    public Store(){}

    @Override
    public String toString() {
        return "Map: " + product +
                " - " + count;
    }
    public void printAll(ObjectInputStream in, ObjectOutputStream out) throws IOException, ClassNotFoundException {
        Object obj = in.readObject();
        out.writeObject(obj);
        String string = obj.toString();
        string = string.replaceAll("=", " - ");
        String[] strings = string.split(", |\\{|\\}");

        System.out.print("Storage:");
        for (int i = 0; i < strings.length; i++)
            System.out.println("\t" + strings[i]);
    }

    public HashMap<String, Integer> remove(ObjectInputStream in, ObjectOutputStream out, String product) throws IOException, ClassNotFoundException {
        HashMap<String, Integer> newMap = (HashMap<String, Integer>) in.readObject();
        newMap.remove(product);
        out.writeObject(newMap);
        return newMap;
    }

    public HashMap<String, Integer> addNew(ObjectInputStream in, ObjectOutputStream out, Store store) throws IOException, ClassNotFoundException {
        HashMap<String, Integer> newMap = (HashMap<String, Integer>) in.readObject();
        if(newMap.containsKey(store.getProduct()))
            newMap.put(store.getProduct(), store.getCount() + newMap.get(store.getProduct()));
        else
            newMap.put(store.product, store.count);
        out.writeObject(newMap);
        return newMap;
    }

    public HashMap<String, Integer> changeCount(ObjectInputStream in, ObjectOutputStream out, Store store) throws IOException, ClassNotFoundException {
        HashMap<String, Integer> newMap = (HashMap<String, Integer>) in.readObject();
        newMap.put(store.getProduct(), store.getCount());
        out.writeObject(newMap);
        return newMap;
    }


    public int getCount(){
        return count;
    }
    public String getProduct(){
        return product;
    }
    public HashMap getStorage(){
        return storageMap;
    }


    public void processStore(ObjectInputStream in, ObjectOutputStream out) throws IOException, ClassNotFoundException {
        Scanner sc = new Scanner(System.in);
        storageMap = new HashMap<>();

        System.out.println("Сколько хочешь ввести товаров?");
        int startNumber = sc.nextInt();

        System.out.println("Введи \"название товара - кол-во\"");
        for (int i = 0; i < startNumber; i++) {
            Store store = new Store(sc.next(), sc.nextInt());
            storageMap.put(store.getProduct(), store.getCount());
        }
        out.writeObject(storageMap);

        int id = 1;
        while(true) {
            System.out.println("1.Вывод всех товаров.\n" +
                    "2.Добавление нового в список.\n" +
                    "3.Удаление товара со склада.\n" +
                    "4.Изменение количества товара.\n" +
                    "0.Выход из программы.");
            id = sc.nextInt();
            if(id != 1 && id != 2 && id != 3 && id != 4)
                break;
            switch (id){
                case 1:
                    printAll(in, out);
                    break;
                case 2:
                    System.out.println("Введи \"название товара - кол-во\"");
                    Store store = new Store(sc.next(), sc.nextInt());
                    storageMap = addNew(in, out, store);
                    produceListeners();
                    break;
                case 3:
                    System.out.println("Какой товар убрать?");
                    storageMap = remove(in, out, sc.next());
                    produceListeners();
                    break;
                case 4:
                    System.out.println("Количество какого товара заменить и на сколько?");
                    store = new Store(sc.next(), sc.nextInt());
                    storageMap = changeCount(in, out, store);
                    produceListeners();
                    break;
            }
        }
    }
    public void produceListeners() throws IOException, ClassNotFoundException {
        for (ProductsListener listener:listeners) {
            listener.productsUpdate();
        }
    }
}