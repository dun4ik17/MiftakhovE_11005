import java.util.Iterator;
import java.util.Scanner;

public class LinkedList<T> implements Iterable<T> {
    static class Node<T>{
        private Node<T> last;
        private T value;
        private int index = -1;

        public Node(Node<T> last, T value,int index){
            this.index = index;
            this.last = last;
            this.value = value;
        }
    }

    public Iterator<T> iterator(){
        return new ListIterator();
    }
    public class ListIterator implements Iterator<T>{
        @Override
        public boolean hasNext() {
            boolean hasNext = head != null;
            if(!hasNext)
                head = getHead;
            return hasNext;
        }

        @Override
        public T next() {
            T item = head.value;
            head = head.last;
            return item;
        }
    }

    private Node<T> head;
    private Node<T> getHead;
    private int counter;

    public int getLength(){
        return counter;
    }
    public void add(T value){
        Node<T> t = new Node<>(head, value, counter);
        head = t;
        getHead = head;
        counter++;
    }
    public T get(int i){
        checkIndex(i);
        while(i != head.index)
            head = head.last;

        T value = head.value;
        head = getHead;
        return value;
    }
    public void remove(int i) {
        checkIndex(i);
        Node<T> headNext = null;
        while(i != head.index){
            head.index -= 1;
            headNext = head;
            head = head.last;
        }

        head.value = null;
        counter--;
        if(headNext != null) {
            head.index -= 1;
            headNext.last = head.last;
            head.index = -1; //так как элемент уже никак не используется и доступа к нему нет
        }
        else
            getHead = head.last;
        head = getHead;
    }

    public void checkIndex(int i){
        if (i < 0 || i > counter)
            throw new IndexOutOfBoundsException();
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        LinkedList<Integer> list = new LinkedList<>();

        for (int i = 0; i < 5; i++)
            list.add(sc.nextInt());
        for (int i = 0; i < list.getLength(); i++)
            System.out.print(list.get(i) + " ");

        list.remove(0);
        list.remove(3);
        System.out.println();

        for (int i = 0; i < list.getLength(); i++)
            System.out.print(list.get(i) + " ");
        System.out.println();
        for (Integer x : list) {
            System.out.print(x + " ");
        }
    }
}