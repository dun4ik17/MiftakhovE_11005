import java.util.Iterator;

public class Stack<T> implements Iterable<T>{
    static class Node<T> {
        private Node<T> next;
        private T value;

        public Node(Node<T> next, T value) {
            this.next = next;
            this.value = value;
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new StackIterator();
    }

    public class StackIterator implements Iterator<T> { //почему со static <T> - красное?
        @Override
        public boolean hasNext() {
            boolean hasNext = headFor != null;
            if(!hasNext)
                headFor = head;
            return hasNext;
        }

        @Override
         public T next() {
            T item = headFor.value;
            headFor = headFor.next;
            return item;
        }
    }

    private int length;
    private Node<T> head;
    private Node<T> headFor; //для постоянного использования foreach, без него foreach работает единожды

    public void push(T value) {
        Node<T> t = new Node<>(head, value);
        head = t;
        headFor = head;
        length++;
    }

    public T pop() {
        if (head == null)
            throw new NullPointerException("Create an object");
        T value = head.value;
        head = head.next;
        length--;
        return value;
    }

    public T peek() {
        return head.value;
    }

    public int size(){
        return length;
    }
}
