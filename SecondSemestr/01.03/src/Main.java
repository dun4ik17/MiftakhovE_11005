import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        //Task 1
        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < 10; i++)
            stack.push(sc.nextInt());

        System.out.println("Peek: " + stack.peek() + "\n" +
                "Pop: ");

        for (int i = 0; i < 10; i++)
            System.out.print(stack.pop() + " ");
        System.out.println();
        //Task 2
        Stack<Integer> stack1 = new Stack<>();

        for (int i = 0; i < 5; i++)
            stack1.push(sc.nextInt());
        for (Integer item : stack1)
            System.out.print(item + " ");
        System.out.println();
        for (Integer item : stack1)
            System.out.print(item + " ");
        //Task 3

    }
}
