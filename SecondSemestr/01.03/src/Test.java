import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Test {
    public static int number;
    public <T> void itachi(T obj){
        System.out.println(obj.getClass());
        System.out.println(obj);
    }
    public static <T> void printAll(T... values){
        for (T i: values) {
            System.out.print(i + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        String s = "This is a String";
        //itachi(s);

        printAll(1, 2, 3, 10);
        printAll(12, "String", 5, 'c',11);

        List<String>[] list = new ArrayList[1];
        List<Integer> ints = Arrays.asList(10, 20 ,30);

        Object[] objects = list;
       // objects[0] = ints;

       // System.out.print(list[0].get(0));
        byte b1 = 3;
        byte b2 = 4;
        byte b3 = (byte)(b1+b2);
        byte b4 = 3 + 4;
        System.out.println(b3);
    }
}
