import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

public class Bot {
    private HashMap<String, Method> commands;
    private Commands[] command;

    public Bot(){
        commands = new HashMap<>();
        command = new Commands[this.getClass().getDeclaredMethods().length];

        for (Method m : this.getClass().getDeclaredMethods()) {
            if(!m.isAnnotationPresent(Commands.class))
                continue;
            Commands cmd = m.getAnnotation(Commands.class);
            for (String name : cmd.aliases()) {
                commands.put(name, m);
            }

            command[cmd.id()] = cmd;
        }
    }

    public int getMethodsQuantity(){
        int size = 0;
        for (Method m : this.getClass().getDeclaredMethods()) {
            if (m.isAnnotationPresent(Commands.class))
                size++;
        }
        return size;
    }

    public String processUserInput(String input){
        if(input.isEmpty())
            return "Я тебя не понимаю, скажи чо-нить по делу";

        String[] split = input.split(" ");
        String command = split[0].toLowerCase();
        String[] args = null;
        try {
            args = Arrays.copyOfRange(split, 1, split.length);
            for (int i = 0; i < args.length; i++) {
                args[i] = args[i].toLowerCase();
            }
        }
        catch (Exception e){
            System.out.println("Ты ввёл без аргументов");
        }
        Method m = commands.get(command);

        if(m == null)
            return "Не понял, ты быканул что ли?";
        if(m.getAnnotation(Commands.class).inProgress())
            return "Сея функция не доработана, пошёл нахуй";
        try {
            return (String) m.invoke(this, (Object) args);
        } catch (Exception e) {
            return "Что-то пошло не так, попробуй заново";
        }
    }

    @Commands(aliases = {"привет", "здаров", "hello"},
            args = "",
            id = 0,
            inProgress = false,
            description = "Поздороваться")
    public String hello(String[] args){
        return "Здравствуй, друг мой";
    }

    @Commands(aliases = {"дата_смерти", "смерть"},
            args = "",
            id = 1,
            inProgress = false,
            description = "Хочешь узнать, когда сдохнешь? :D")
    public String printDeathDate(String[] args) {
        Random random = new Random();
        Date date = new Date();
        String dateAr[] = date.toString().split(" ");
        String strYear = dateAr[dateAr.length - 1];
        Formatter f = new Formatter();

        int day = random.nextInt(30) + 1;
        int month = random.nextInt(12);
        int year = Integer.parseInt(strYear) + 35 + random.nextInt(30) + 1;

        if(month == 1 && day > 28)
            day = 28;
        String strMonth = DataBase.months[month];

        f.format("%d %s %d", day, strMonth, year);
        return f.toString();
    }

    @Commands(aliases = {"персонажи_наруто", "наруто", "naruto"},
            args = "Название клана (Учиха, Каге, Акатцки, Удзумаки)",
            id = 2,
            inProgress = false,
            description = "Выводит список членов определённого клана из Наруто")
    public String printPerson(String args[]){
        int i = 0;
        StringBuilder result = new StringBuilder();

        while(i < DataBase.clans.length){
            if(args[0].toLowerCase().equals(DataBase.clans[i].toLowerCase()))
                break;
            if(i == DataBase.clans.length - 1)
                return "Такого в Наруто нет (";
            i++;
        }

        result.append(DataBase.clans[i] + ":\n");
        for (int j = 0; j < DataBase.persons[i].length; j++) {
            result.append("\t" + DataBase.persons[i][j] + "\n");
        }

        return result.toString();
    }

    @Commands(aliases = {"фибоначчи", "выведи_фибоначчи", "fibonacci"},
            args = "Число n",
            id = 3,
            inProgress = false,
            description = "Выводит первых n-чисел фибоначчи")
    public String printFibNumber(String[] args) { //метод вывода чисел Фиббоначи до их n-го числа
        int n;
        try {
            n = Integer.parseInt(args[0]);
        } catch (Exception e) {
            return "Ты не ввёл число или же оно слишком большое";
        }
        int fibArray[] = new int[n];
        fibArray[1] = 1;
        for (int i = 2; i < fibArray.length; i++)
            fibArray[i] = fibArray[i - 1] + fibArray[i - 2];
        return Arrays.toString(fibArray);
    }

    @Commands(aliases = {"комплимент", "я_не_красивый", "сделай_комплимент"},
            args = "",
            id = 4,
            inProgress = false,
            description = "Вкидывает комплимент")
    public String printCompliment(String[] args){
        Random ran = new Random();
        int n = ran.nextInt(7);
        return DataBase.compliments[n].toString();
    }

    @Commands(aliases = {"преподы", "список_преподов", "teacher"},
            args = "",
            id = 5,
            inProgress = true,
            description = "Выводит список преподов")
    public String printTeachers(String[] args) {
        StringBuilder strBuilder = new StringBuilder();
        for (int i = 0; i < DataBase.teachers.length; i++) {
            strBuilder.append("\t");
            strBuilder.append(DataBase.teachers[i]).append(" - ").append(DataBase.whatTeacherIs[i]).append(".\n");
        }
        return strBuilder.toString();
    }

    @Commands(aliases = {"помоги", "помощь" ,"help" ,"help_me"},
            args = "Нужные комманды/Без них",
            id = 6,
            inProgress = false,
            description = "Выводит список команд")
    public String help(String[] args){
        StringBuilder builder = new StringBuilder("Я умею в такие команды: \n");

        for(Method m : this.getClass().getDeclaredMethods()) {
            if (!m.isAnnotationPresent(Commands.class))
                continue;

            Commands cmd = m.getAnnotation(Commands.class);
            if(cmd.inProgress())
                continue;
            builder.append(Arrays.toString(cmd.aliases())).append("\n");
            if (args == null) {
                builder.append("\n");
                break;
            }
            for (int i = 0; i < args.length; i++) {
                for (String name : cmd.aliases()) {
                    if(args[i].equals(name)){
                        builder.append(" - ").append(cmd.description()).
                                append(" (").append(cmd.args()).append(")\n");
                        break;
                    }
                }
            }
        }
        return builder.toString();
    }

    @Commands(aliases = {"commands", "комманды", "комманда"},
            args = "",
            id = 7,
            inProgress = false,
            description = "Выводит список реализованных комманд," +
                    "\nа после тех, что в разработке")
    public String getCommands(String[] args){
        int size = getMethodsQuantity();
        int firstId = Integer.MAX_VALUE;
        int[] methodsId = new int[size];

        for (int i = 0; i < size; i++)
            methodsId[i] = -1;

        StringBuilder builder = new StringBuilder("Реализованные комманды: \n");

        int i = 0;
        for (Method m : this.getClass().getDeclaredMethods()) {
            if (!m.isAnnotationPresent(Commands.class))
                continue;

            Commands cmd = m.getAnnotation(Commands.class);

            if(cmd.inProgress()) {
                firstId = getTrueId(methodsId);
                methodsId[firstId] = cmd.id();
            }
            else {
                methodsId[i] = cmd.id();
                i++;
            }
        }

        for (int j = 0; j < size; j++) {
            Commands cmd = command[methodsId[j]];

            if(j < firstId)
                builder.append("\t").append(Arrays.toString(cmd.aliases())).append("\n");
            else if(j == firstId){
                builder.append("Комманды в разработке: ").append("\n");
                builder.append("\t").append(Arrays.toString(cmd.aliases())).append("\n");
            }
            else
                builder.append("\t").append(Arrays.toString(cmd.aliases())).append("\n");
        }
        return builder.toString();
    }
    public int getTrueId(int[] methodsId) {
        for (int i = methodsId.length - 1; i >= 0; i--) {
            if (methodsId[i] == -1)
                return i;
        }
        return -1;
    }
}
