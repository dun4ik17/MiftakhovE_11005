import java.util.*;

public class Main {
    public static boolean tryParseInt(String in){ // Функция для Task 2
        try{
            Integer.parseInt(in);
        }catch(Exception e){
            return false;
        }
        return true;
    }
    public static boolean checkOperator(String in){ // Функция для Task 2
        return in.equals("+") || in.equals("-")
                || in.equals("*") || in.equals("/");
    }
    public static int doOperation(int first, int second, String ch){ // Функция для Task 2
        return switch (ch) {
            case "+" -> first + second;
            case "-" -> first - second;
            case "*" -> first * second;
            default -> first / second;
        };
    }
    public static boolean checkBrackets(char start, char end, Stack<Character> stack,
                                        char ch) throws Exception { // Функция для Task 1
        if(ch == start)
            stack.push(ch);
        else if(ch == end)
            if(stack.peek() == start)
                stack.pop();
            else
                throw new Exception("Incorrect string");
        else
            return false;
        return true;
    }
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);

        //Task 1
        {
            Stack<Character> stack = new Stack<>();
            char[] chars = sc.nextLine().toCharArray();

            for (int i = 0; i < chars.length; i++) {
                char ch = chars[i];

                if (checkBrackets('{', '}', stack, ch)) {}
                else if (checkBrackets('(', ')', stack, ch)) {}
                else if (checkBrackets('[', ']', stack, ch)) {}
            }
            if(stack.isEmpty())
                System.out.println("This is a right string");
            else
                throw new Exception("Incorrect string");
        }
        //Task 2
        {
            Stack<Integer> stack = new Stack<>();
            String[] chars = sc.nextLine().split(" ");

            for (int i = 0; i < chars.length; i++) {
                String ch = chars[i];

                if(ch.equals(""))
                    continue;

                if(tryParseInt(ch))
                    stack.add(Integer.parseInt(ch));
                else if(checkOperator(ch)) {

                    if (!stack.isEmpty()) {
                        int result = stack.pop();
                        if(!stack.isEmpty())
                            result = doOperation(result, stack.pop(), ch);
                        stack.add(result);
                    }
                    else
                        throw new Exception("Incorrect expression");

                }
                else
                    throw new Exception("Incorrect expression");
            }
            System.out.println(stack.pop());
        }
    }
}