import java.util.*;

public class Main {
    public static boolean tryParseInt(String s){
        try{
            Integer.parseInt(s);
        } catch (Exception e){
            return false;
        }
        return true;
    }
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        System.out.println("Task 1");
        {
            Map<String, Integer> dictionary = new HashMap<>();
            String[] strs = sc.nextLine().split(" ");
            System.out.println();

            for (int i = 0; i < strs.length; i++) {
                String str = strs[i];

                if (str.equals(""))
                    continue;

                if (dictionary.containsKey(str)) {
                    int value = dictionary.get(str) + 1;
                    dictionary.put(str, value);
                } else
                    dictionary.put(str, 1);
            }

            for (Map.Entry<String, Integer> entry : dictionary.entrySet()) {
                System.out.format("%s %d\n", entry.getKey(), entry.getValue());
            }
        }
        System.out.println("\nTask 2");
        {
            Map<String, Map<String, Integer>> buyerMap = new HashMap<>();

            while (sc.hasNextLine()){
                String[] words = sc.nextLine().split(" ");
                if(words.length != 3)
                    break;
                if(!buyerMap.containsKey(words[0])){
                    Map<String, Integer> ordersMap = new HashMap<>();
                    ordersMap.put(words[1], Integer.parseInt(words[2]));
                    buyerMap.put(words[0], ordersMap);
                    continue;
                }

                Map<String, Integer> ordersMap = buyerMap.get(words[0]);
                if(!ordersMap.containsKey(words[1])) {
                    ordersMap.put(words[1], Integer.parseInt(words[2]));
                }
                else {
                    int count = ordersMap.get(words[1]);
                    count += Integer.parseInt(words[2]);
                    ordersMap.put(words[1], count);
                }
            }

            for (Map.Entry<String, Map<String, Integer>> entry: buyerMap.entrySet()) {
                System.out.format("%s: \n", entry.getKey());
                for (Map.Entry<String, Integer> entryInternal : entry.getValue().entrySet()) {
                    System.out.format("%s - %d\n", entryInternal.getKey(), entryInternal.getValue());
                }
                System.out.println();
            }
        }
    }
}