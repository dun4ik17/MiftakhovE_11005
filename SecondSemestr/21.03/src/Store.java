import java.io.*;
import java.util.*;
public class Store implements Serializable {
    private String product;
    private int count;
    static HashMap<String, Integer> storageMap = new HashMap<>();

    public Store(String product, int count){
        this.product = product;
        this.count = count;
    }
    public Store(){

    }
    @Override
    public String toString() {
        return "Map: " + product +
                " - " + count;
    }
    public void printAll(ObjectInputStream in, ObjectOutputStream out) throws IOException, ClassNotFoundException {
        Object obj = in.readObject();
        out.writeObject(obj);
        String string = obj.toString();
        string = string.replaceAll("=", " - ");
        String[] strings = string.split(", |\\{|\\}");

        System.out.print("Storage:");
        for (int i = 0; i < strings.length; i++)
            System.out.println("\t" + strings[i]);
    }

    public void remove(ObjectInputStream in, ObjectOutputStream out, String product) throws IOException, ClassNotFoundException {
        HashMap<String, Integer> newMap = (HashMap<String, Integer>) in.readObject();
        newMap.remove(product);
        out.writeObject(newMap);
    }

    public void addNew(ObjectInputStream in, ObjectOutputStream out, Store store) throws IOException, ClassNotFoundException {
        HashMap<String, Integer> newMap = (HashMap<String, Integer>) in.readObject();
        if(newMap.containsKey(store.getProduct()))
            newMap.put(store.getProduct(), store.getCount() + newMap.get(store.getProduct()));
        else
            newMap.put(store.product, store.count);
        out.writeObject(newMap);
    }

    public void changeCount(ObjectInputStream in, ObjectOutputStream out, Store store) throws IOException, ClassNotFoundException {
        HashMap<String, Integer> newMap = (HashMap<String, Integer>) in.readObject();
        newMap.put(store.getProduct(), store.getCount());
        out.writeObject(newMap);
    }


    public int getCount(){
        return count;
    }
    public String getProduct(){
        return product;
    }

    public void setCount(int count){
        this.count = count;
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Store main = new Store();
        Scanner sc = new Scanner(System.in);
        HashMap<String, Integer> storageMap = new HashMap<>();
        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(".\\src\\Storage.txt"));
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(".\\src\\Storage.txt"));

        System.out.println("Сколько хочешь ввести товаров?");
        int startNumber = sc.nextInt();

        System.out.println("Введи \"название товара - кол-во\"");
        for (int i = 0; i < startNumber; i++) {
            Store store = new Store(sc.next(), sc.nextInt());
            storageMap.put(store.getProduct(), store.getCount());
        }
        out.writeObject(storageMap);
        int id = 1;
        while(true) {
            System.out.println("1.Вывод всех товаров.\n" +
                    "2.Добавление нового в список.\n" +
                    "3.Удаление товара со склада.\n" +
                    "4.Изменение количества товара.\n" +
                    "0.Выход из программы.");
            id = sc.nextInt();
            if(id != 1 && id != 2 && id != 3 && id != 4)
                break;
            switch (id){
                case 1:
                    main.printAll(in, out);
                    break;
                case 2:
                    System.out.println("Введи \"название товара - кол-во\"");
                    Store store = new Store(sc.next(), sc.nextInt());
                    main.addNew(in, out, store);
                    break;
                case 3:
                    System.out.println("Какой товар убрать?");
                    main.remove(in, out, sc.next());
                    break;
                case 4:
                    System.out.println("Количество какого товара заменить и на сколько?");
                    store = new Store(sc.next(), sc.nextInt());
                    main.changeCount(in, out, store);
                    break;
            }
        }
        in.close();
        out.close();
    }
}