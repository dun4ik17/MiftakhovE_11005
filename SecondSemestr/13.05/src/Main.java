import java.io.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        {
            //Task 1
            StringBuilder builder = new StringBuilder("");
            while(sc.hasNextLine() && !sc.hasNext("stop")){
                builder.append(sc.next()).append(" ");
            }

            String[] strings = builder.toString().split(" ");
            Stream<String> stream = Arrays.stream(strings);
            stream.filter(x -> x.startsWith("a"))
                    .sorted((a, b) -> Integer.compare(a.length(), b.length()))
                    .forEach(System.out::println);

        }
        {
            //Task 2
            int[] array = new int[15];
            for (int i = 0; i < 15; i++)
                array[i] = sc.nextInt();

            IntStream stream = Arrays.stream(array);
            stream.boxed().
                    sorted(Comparator.comparingInt(x -> x % 10))
                    .mapToInt(x -> x)
                    .map(x -> x / 10)
                    .distinct()
                    .forEach(System.out::println);

        }
        {
           //Task 3
            File file = new File("src\\task3.txt");
            BufferedReader reader = new BufferedReader(new FileReader(file));

            String[][] array = new String[100][2];
            int i = 0;

            while(reader.ready()) {
                String[] inArr = reader.readLine().split("\\|");
                array[i][0] = inArr[0];
                array[i][1] = inArr[1];
                i++;
            }

            Stream<String[]> stream =  Arrays.stream(array);
            var map = stream.collect(Collectors.groupingBy(s -> s[0]))
                    .entrySet().stream()
                    .collect(Collectors.toMap(
                            x -> x.getKey(),
                            x -> x.getValue().stream()
                                    .reduce(0,
                                            (y, z) -> y + Integer.parseInt(z[1]),
                                            (y, z) -> y + z)));

            for (Map.Entry<String, Integer> entry : map.entrySet()) {
                System.out.println(entry.getKey() + " - " + entry.getValue());
            }

            reader.close();
        }
    }
}
