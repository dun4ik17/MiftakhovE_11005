import java.util.ArrayList;

public class Dispatcher extends Person {
    private ArrayList<Race> races;
    private ArrayList<Driver> drivers; //водители на базе

    public Dispatcher(String name, int age, String sex, ArrayList<Race> races){
        super(name, age, sex);
        this.races = races;
    }

    public ArrayList<Driver> getDrivers(){
        return drivers;
    }

    public ArrayList<Race> getRaces(){
        return races;
    }

    public void specialAction(){ //показывает заявки на рейсы
        System.out.println("Заявки на рейсы: \n" +
                "Id\tДлина_пути\tЖелаемое авто\tСтатус");
        for (int i = 0; i < races.size(); i++) {
            Race race = races.get(i);
            System.out.print(race.getRaceId() + "\t" + race.getLength() + "\t\t\t" + race.getPreferableCar());
            if(race.getStatus())
                System.out.println("\t\t\t \tВыполнен");
            else
                System.out.println("\t\t\t \tНе выполнен");
        }
    }

    public void giveRace(int driverId, Race race){
        drivers.get(driverId).setRace(race);
    }

    public void giveAuto(int driverId, Car car){
        drivers.get(driverId).setCar(car);
    }

    public void addNewDriver(Driver driver){
        drivers.add(driver);
    }

    public void fireDriver(int id){
        drivers.remove(drivers.get(id));
    }
}
