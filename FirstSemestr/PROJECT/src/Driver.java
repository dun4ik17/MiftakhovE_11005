import java.util.ArrayList;

public class Driver extends Person {
    private int id;
    private Car car;
    private ArrayList<Race> races;
    private boolean needRepairing = false;

    public int getId(){
        return id;
    }

    public void setId(int id){
        this.id = id;
    }

    public Driver(String name, int age, String sex){
        super(name, age, sex);
    }

    public Car getCar(){
        return car;
    }

    public void setRace(Race race){
        races.add(race);
    }

    public void setCar(Car car){
        this.car = car;
    }

    public void completeRace(int id){
        races.get(id).setStatus(true);
    } //отметка о выполнении рейса

    public void specialAction(){ //заявка на ремонт авто
        System.out.println(getName() + "делает заявку на ремонт автомобиля");
        needRepairing = true;
    }
    public void carIsFine(){
        if(car.getHealth() == 100){
            needRepairing = false;
        }
    }

}
