public class Car{
    private int carId;
    private int typeId;
    private String type;
    private String number;
    private int health;

    public Car(String number, String type){
        this.number = number;
        this.type = type;
        this.health = 100;
    }

    public void setCarId(int carId){
        this.carId = carId;
    }

    public int getCarId(){
        return carId;
    }

    public int getHealth(){
        return health;
    }

    public String getType(){
        return type;
    }

    public String getNumber(){
        return number;
    }

    public void repairCar(){
        this.health = 100;
    }

    public void crashCar(int c){
        this.health -= c;
        if(this.health < 0)
            this.health = 0;
    }
}
