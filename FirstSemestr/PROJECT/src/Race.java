public class Race {
    private int raceId;
    private boolean status;
    private int length;
    private String preferableCar;

    public Race(int length, String preferableCar){
        this.length = length;
        this.status = false;
        this.preferableCar = preferableCar;
    }

    public int getRaceId(){
        return raceId;
    }

    public void setRaceId(int raceId){
        this.raceId = raceId;
    }

    public int getLength(){
        return length;
    }

    public boolean getStatus(){
        return status;
    }

    public String getPreferableCar(){
        return preferableCar;
    }

    public void setStatus(boolean status){
        this.status = status;
    }
}
