import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    static Scanner sc = new Scanner(System.in);
    static ArrayList<Car> cars = new ArrayList<>();
    static ArrayList<Race> races = new ArrayList<>();
    static Dispatcher dispatcher;
    static String[] availableTypes = new String[4];

    public static void writeLine(){
        System.out.println("====================\n" +
                "");
    }

    public static int checkTypeId(int typeId, int length){
        typeId--;
        if(typeId < 0){
            System.out.println("Вам повезло, что я исправил вашу ошибку :)");
            typeId = 0;
        }
        else if(typeId >= length ) {
            System.out.println("Вам повезло, что я исправил вашу ошибку :)");
            typeId = length - 1;
        }
        return typeId;
    }
    public static void main(String[] args) {
        boolean answer;
        Base base = new Base();
        sc.nextLine();

        availableTypes[0] = "Легковой";
        availableTypes[1] = "Грузовой";
        availableTypes[2] = "Общественный";
        availableTypes[3] = "Мотоцикл";

        System.out.println("\tДобро пожаловать в симулятор Автобазы!\n" +
                "\tМеня зовут Альфред, я ваш личный ассистент" +
                "\tДля начала работы, введите данные автомобилей\n");

        while(true) {
            answer = false;
            writeLine();
            System.out.println("Выберите тип автомобиля:\n" +
                    "1.Легковой\n" +
                    "2.Грузовой\n" +
                    "3.Общетсвенный\n" +
                    "4.Мотоцикл");

            int count = 0;
            int typeId = sc.nextInt();
            typeId = checkTypeId(typeId, 4);
            String type = availableTypes[typeId];
            System.out.println("Тип авто выбран, какое количество таких авто нужно?");
            count = sc.nextInt();

            for (int i = 0; i < count; i++) {
                System.out.println("Введите номер автомобиля (XXXXX)");
                String number = sc.next();
                Car car = new Car(number, type);
                cars.add(car);
                int carId = cars.size();
                carId = checkTypeId(carId, cars.size());
                car.setCarId(carId);
            }
            System.out.println("Хотите добавить ещё один тип?\n" +
                    "(y/n)");
            while (true){
                String ans = sc.next();

                if(ans.equals("y")) {
                    answer = true;
                    break;
                }
                else if(ans.equals("n")) {
                    answer = false;
                    break;
                }
                else
                    System.out.println("Не понимаю, пожалуйста введите один из вариантов (y/n)");
            }
            if(!answer)
                break;
        }

        base.addCars(cars);

        while(true){
            System.out.println("Теперь введите данные заявки на рейс\n" +
                    "(Длина/Желаемый тип авто\n" +
                    "1.Легковой\n" +
                    "2.Грузовой\n" +
                    "3.Общетсвенный\n" +
                    "4.Мотоцикл");

            int length = sc.nextInt();
            int idType = sc.nextInt();
            idType = checkTypeId(idType, 4);
            String preferableCar = availableTypes[idType];
            Race race = new Race(length, preferableCar);
            races.add(race);
            int raceId = races.size();
            raceId = checkTypeId(raceId, races.size());
            race.setRaceId(raceId);
            System.out.println("Хотите сделать ещё одну заявку?(y/n)");
            String ans = sc.next();
            if(ans.equals("y")){
                continue;
            }
            else if(ans.equals("n")){
                break;
            }
            else
                System.out.println("Не понимаю, пожалуйста введите один из вариантов (y/n)");
        }

        System.out.println("Замечательно, теперь поставим на работу Диспетчера\n" +
                "Пожалуйста, введите данные о нём (Имя/возраст/пол)");

        String name = sc.next();
        int age = sc.nextInt();
        String sex = sc.next();

        dispatcher = new Dispatcher(name, age, sex, races);

        base.startProcess();
    }
}
