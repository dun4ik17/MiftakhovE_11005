import java.util.ArrayList;
import java.util.Scanner;

public class Base {
    Dispatcher dispatcher = Main.dispatcher;

    private ArrayList<Car> cars;//доступные машины на базе

    public ArrayList<Car> getCars(){
        return cars;
    }

    public void addCars(ArrayList<Car> cars){
        this.cars = cars;
    }

    public int[] carsAmount = new int[4];

    String[] dispatcherFunctions = new String[6];

    public int findId(int i){
        if(cars.get(i).getType().equals("Легковой"))
            return 0;
        else if(cars.get(i).getType().equals("Грузовой"))
            return 1;
        else if(cars.get(i).getType().equals("Общественный"))
            return 2;
        else
            return 3;
    }

    public void findAmount(){
        for (int j = 0; j < cars.size(); j++) {
            carsAmount[findId(j)]++;
        }
    }
    public void showFreeCarsAmount(){
        Main.writeLine();
        System.out.println("Всего авто на автобазе: ");
        for (int i = 0; i < carsAmount.length; i++) {
            if(findId(i) != 2){
                System.out.println(Main.availableTypes[i] + ": \t\t" + carsAmount[i]);
            }
            else
                System.out.println(Main.availableTypes[i] + ": \t" + carsAmount[i]);
        }
    }
    public void carsInfo(){
        Main.writeLine();
        System.out.println("Информация об авто: \n" +
                "Id\t Тип авто\t Номер авто\t Состояние");
        for (int i = 0; i < cars.size(); i++) {
            Car car = cars.get(i);
            if(findId(i) != 2) {
                System.out.println(car.getCarId() +" \t" + car.getType()
                        + "\t\t" + car.getNumber() + car.getHealth() + "%");
            }
            else
                System.out.println(car.getCarId() +" \t" + car.getType()
                        + "\t" + car.getNumber() + car.getHealth() + "%");
        }
    }

    public void startProcess(){
        Scanner sc = new Scanner(System.in);
        boolean answer = false;
        dispatcherFunctions[0] = "1.Нанять нового водителя";
        dispatcherFunctions[1] = "2.Посмотреть заявки";
        dispatcherFunctions[2] = "3.Уволить водителя";
        dispatcherFunctions[3] = "4.Выдать водителю авто";
        dispatcherFunctions[4] = "5.Выдать водителю заявку/заявки на рейс";
        dispatcherFunctions[5] = "6.Данные об авто на автобазе";

        System.out.println("На Автобазу прибыл новый главный Диспетчер " + dispatcher.getName() + "\n" +
                "\t\t\t\t\t\t\t\t\t\t" + dispatcher.getAge() + " лет\n" +
                "\t\t\t\t\t\t\t\t\t\t" + "Пол: " + dispatcher.getSex());

        findAmount();
        carsInfo();

        do {
            System.out.println("Вы главный Диспетчер, ваши доступные действия:\n" +
                    dispatcherFunctions[0] + "\n" +
                    dispatcherFunctions[1] + "\n" +
                    dispatcherFunctions[2] + "\n" +
                    dispatcherFunctions[3] + "\n" +
                    dispatcherFunctions[4] + "\n" +
                    dispatcherFunctions[5]);

            int requestId = sc.nextInt();
            requestId = Main.checkTypeId(requestId, dispatcherFunctions.length);
            switch (requestId){
                case 0:
                    System.out.println("Введи имя, возраст, пол");
                    Driver driver = new Driver(sc.next(), sc.nextInt(), sc.next());
                    dispatcher.addNewDriver(driver);
                    int driverId = dispatcher.getDrivers().size();
                    driverId = Main.checkTypeId(driverId, dispatcher.getDrivers().size());
                    driver.setId(driverId);
                    break;
                case 1:
                    dispatcher.specialAction();
                case 2:
                    System.out.println("Введи id водителя");
                    driverId = sc.nextInt();
                    driverId = Main.checkTypeId(driverId, dispatcher.getDrivers().size());
                    dispatcher.fireDriver(driverId);
                case 3:
                    System.out.println("Введи id водителя");
                    driverId = sc.nextInt();
                    driverId = Main.checkTypeId(driverId, dispatcher.getDrivers().size());
                    carsInfo();
                    System.out.println("Введи id авто для выбранного водителя");
                    int carId = sc.nextInt();
                    carId = Main.checkTypeId(carId, cars.size());
                    Car car = cars.get(carId);
                    dispatcher.giveAuto(driverId, car);
                case 4:
                    System.out.println("Введи id водителя");
                    driverId = sc.nextInt();
                    driverId = Main.checkTypeId(driverId, dispatcher.getDrivers().size());
                    dispatcher.specialAction();
                    System.out.println("Введи id заявки/заявок на рейс");
                    boolean answer1 = false;
                    do {
                        int raceId = sc.nextInt();
                        raceId = Main.checkTypeId(raceId, dispatcher.getRaces().size());
                        Race race = dispatcher.getRaces().get(raceId);
                        dispatcher.giveRace(driverId,race);
                        System.out.println("Хочешь ли ты дать этому водителю ещё заявку на рейс?(y/n)");
                        while(true){
                            String ans = sc.next();

                            if(ans.equals("y")) {
                                answer1 = true;
                                break;
                            }
                            else if(ans.equals("n")) {
                                answer1 = false;
                                break;
                            }
                            else
                                System.out.println("Не понимаю, пожалуйста введи один из вариантов (y/n)");
                        }
                    }while(answer1);
                case 5:
                    showFreeCarsAmount();
                    carsInfo();
                default:
                    System.out.println("Такого зароса нет :(");
            }
            System.out.println("Будут ли ещё запросы?");
            while (true){
                String ans = sc.next();

                if(ans.equals("y")) {
                    answer = true;
                    break;
                }
                else if(ans.equals("n")) {
                    answer = false;
                    break;
                }
                else
                    System.out.println("Не понимаю, пожалуйста введи один из вариантов (y/n)");
            }
        }while(answer);
    }
}
