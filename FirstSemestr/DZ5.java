import java.util.Scanner;

public class DZ5 {
    public static int firstTask(int n){
        int c=0;
        while(n>0){
            c++;
            n/=10;
        }
        return c;
    }
    public static String secondTask(String str){
        String str1="";
        for (int i = str.length()-1; i >=0 ; i--) {
            str1+=str.charAt(i);
        }
        return str1;
    }
    public static int thirdTask(int[] ar){
        int max=Integer.MIN_VALUE;
        for (int i = 0; i < ar.length; i++) {
            if(ar[i]>max) max=ar[i];
        }
        return max;
    }
    public static int[] fourthTask(int[] ar1, int[] ar2){
        int n=ar1.length+ar2.length;
        int[] ar=new int[n];
        for (int i = 0; i < ar1.length; i++) {
            ar[i]=ar1[i];
        }
        for (int i = 0; i < ar2.length ; i++) {
            ar[i+ ar1.length]=ar2[i];
        }
        return ar;
    }
    public static int[][] fifthTask(int[][] ar, int n, int m){
        int[][] mat=new int[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                mat[i][j]=ar[j][i];
            }
        }
        return mat;
    }
    public static int[][] sixthTask(int[][] mat1, int[][] mat2, int f1, int f2){
        int[][] mat3=new int[f1][f2];
        for (int i = 0; i < f1; i++) {
            for (int j = 0; j < f2; j++) {
                mat3[i][j]=mat1[i][j]+mat2[i][j];
            }
        }
        return mat3;
    }
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Введи число для первой задачи");
        int n=sc.nextInt();
        System.out.println(firstTask(n));
        System.out.println("Введи строку для второй задачи");
        sc.nextLine();
        String str=sc.nextLine();
        System.out.println(secondTask(str));
        System.out.println("Введи кол-во эл-ов в массиве");
        int m=sc.nextInt();
        int[] ar1=new int[m];
        System.out.println("Введи элементы массива для третьей задачи");
        for (int i = 0; i < m; i++) ar1[i]=sc.nextInt();
        System.out.println(thirdTask(ar1));
        System.out.println("Введи два числа m и n(кол-во эл-ов каждого массива)");
        n=sc.nextInt();
        m=sc.nextInt();
        int[] arr1=new int[n];
        int[] arr2=new int[m];
        System.out.println("Введи значения элементов n-го массива");
        for (int i = 0; i < n; i++) arr1[i]=sc.nextInt();
        System.out.println("Введи значения элементов m-го массива");
        for (int i = 0; i < m; i++) arr2[i]=sc.nextInt();
        int[] arr= fourthTask(arr1, arr2);
        System.out.println("Ваш массив");
        for (int i = 0; i < arr.length; i++) System.out.print(arr[i]+" ");
        System.out.println();
        System.out.println("Введи порядки матрицы(a, b)");
        int a=sc.nextInt();
        int b=sc.nextInt();
        int[][] matrix=new int[a][b];
        System.out.println("Введи значения матрицы");
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < b; j++) {
                matrix[i][j]=sc.nextInt();
            }
        }
        matrix=fifthTask(matrix, a, b);
        System.out.println("Транспонирование матрицы");
        for (int i = 0; i < matrix.length; i++) {
            System.out.println();
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j]+" ");
            }
        }
        System.out.println();
        System.out.println("Введи порядки двух матрицы (числа f1, f2)");
        int f1=sc.nextInt();
        int f2=sc.nextInt();
        int[][] matrix1=new int[f1][f2];
        int[][] matrix2=new int[f1][f2];
        System.out.println("Введи значения для первой матрицы");
        for (int i = 0; i < f1; i++) {
            for (int j = 0; j < f2; j++) {
                matrix1[i][j]=sc.nextInt();
            }
        }
        System.out.println("Введи значения для второй матрицы");
        for (int i = 0; i < f1; i++) {
            for (int j = 0; j < f2; j++) {
                matrix2[i][j]=sc.nextInt();
            }
        }
        int[][] matrix3=sixthTask(matrix1, matrix2, f1, f2);
        for (int i = 0; i < f1; i++) {
            System.out.println();
            for (int j = 0; j < f2; j++) {
                System.out.print(matrix3[i][j]+" ");
            }
        }
    }
}
