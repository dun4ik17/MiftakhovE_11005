import java.util.Scanner;

public class DZ4 {
    static int n;
    static int[] ar;
    public static void qsort(int left, int right) {
        int l=left;
        int r=right;
        int st=ar[(l+r)/2];
        while(l<=r){
            while(ar[l]<st) l++;
            while(ar[r]>st) r--;
            if(l<=r){
                int t=ar[l];
                ar[l]=ar[r];
                ar[r]=t;
                l++;
                r--;
            }
        }
        if(left<r) qsort(left, r);
        if(right>l) qsort(l, right);
    }
    public static int numberWords(String str){
        int c=0;
        for (int i = 0; i < str.length(); i++) {
            while (str.charAt(i) == ' ') {
                i++;
                if (i == str.length()) break;
            }
            while(str.charAt(i) != ' '){
                i++;
                if(i == str.length()){
                    c++;
                    break;
                }
                if(str.charAt(i) == ' ') c++;
            }
        }
        return c;
    }
    public static int binSearch(int key){
        int l=0;
        int r=n-1;
        int mid=0;
        boolean task=false;
        while(!task){
            mid=(l+r)/2;
            if(key==ar[mid]){
                task=true;
            }
            else if(l==r) return -1;
            else if(key<ar[mid]) r=mid-1;
            else l=mid+1;
        }
        return mid;
    }
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Введи кол-во элементов массива");
        n=sc.nextInt();
        ar=new int[n];
        System.out.println("Введи элеметы массива");
        for (int i = 0; i < n; i++) {
            ar[i]=sc.nextInt();
        }
        qsort(0,n-1);
        System.out.println("Отсортированный массив");
        for (int i = 0; i < n; i++) {
            System.out.print(ar[i]+" ");
        }
        System.out.println();
        System.out.println("Введи значение элемента, индекс которого хочешь найти");
        int val=sc.nextInt();
        System.out.println(binSearch(val));
        System.out.println("Введи строку");
        sc.nextLine();
        String str=sc.nextLine();
        String[] words=new String[numberWords(str)];
        int l=0;
        String str1="";
        for (int i = 0; i < str.length(); i++) {
            while (str.charAt(i) == ' ') {
                i++;
                if (i == str.length()) break;
            }
            while(str.charAt(i) != ' '){
                str1+=str.charAt(i);
                i++;
                if(i == str.length() && str1!="") words[l] = str1;
                if(i == str.length()) break;
                if(str.charAt(i) == ' '){
                    words[l] = str1;
                    l++;
                    str1="";
                }
            }
        }
        for (int i = 0; i < words.length; i++) { // неэффективно, O(n^3)
            if(i==words.length-1) break;
            for (int j = i+1; j < words.length; j++) {
                for (int k = 0; k < words[i].length(); k++) {
                    if(words[i].charAt(k)<words[j].charAt(k)) break;
                    else if(words[i].charAt(k)==words[j].charAt(k)) continue;
                    else {
                        String t=words[i];
                        words[i]=words[j];
                        words[j]=t;
                        break;
                    }
                }
            }
        }
        for (int i = 0; i < words.length; i++) {
            System.out.print(words[i] +" ");
        }
        System.out.println();
        System.out.println("Задача 3");
        System.out.println("Введи порядок квадратной матрицы");
        int m=sc.nextInt();
        int[][] matrix=new int[m][m];
        System.out.println("Введи значения матрицы");
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < m; j++) {
                matrix[i][j]=sc.nextInt();
            }
        }
        int count=0;
        boolean[][] truematrix=new boolean[m][m];
        for (int i = 0; i < m; i++) {
            if(matrix[i][i]%2==0){
                count++;
                truematrix[i][i]=true;
            }
            if(matrix[m-i-1][i]%2==0 && !truematrix[m-i-1][i]) count++;
        }
        if(count==m/2) System.out.println("YES");
        else System.out.println("NO");
    }
}
