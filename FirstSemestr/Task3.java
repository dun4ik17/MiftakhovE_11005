import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Введи числа для 1 и 2 заданий");
        int n=sc.nextInt();
        int m=sc.nextInt();
        int[][] ar1=new int[n][n];
        int[][] ar2=new int[n][n];
        System.out.println("Введи значения элементов 1-ой матрицы");
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                ar1[i][j]=sc.nextInt();
            }
        }
        System.out.println();
        System.out.println("Введи значения элементов 2-ой матрицы");
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                ar2[i][j]=sc.nextInt()+ar1[i][j];
            }
        }
        System.out.println("Результат 1: ");
        for (int i = 0; i < n; i++) {
            System.out.println();
            for (int j = 0; j < n; j++) {
                System.out.print(ar2[i][j]+" ");
            }
        }



        System.out.println();
        System.out.println("Введи значения матрицы для 2-ого задания");
        int[][] matrix1=new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                matrix1[i][j]=sc.nextInt();
            }
        }
        System.out.println();
        int[][] matrix2=new int[m][n];
        System.out.println("Транспонирование:");
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                matrix2[i][j]=matrix1[j][i];
            }
        }
        for (int i = 0; i < m; i++) {
            System.out.println();
            for (int j = 0; j < n; j++) {
                System.out.print(matrix2[i][j]+" ");
            }
        }


        System.out.println();
        System.out.println("Введи строку:");
        String str=sc.next();
        char[] ar=new char[str.length()];
        for (int i = 0; i < ar.length; i++) ar[i]=str.charAt(i);
        for (int i = 0; i < ar.length; i++) {
            for (int j = i+1; j < ar.length; j++) {
                if(ar[i]==ar[j]){
                    ar[j]=0;
                }
            }
        }
        int c=0;
        System.out.println("Кол-во уникальных элементов");
        for (int i = 0; i < ar.length; i++) {
            if(ar[i]!=0) c++;
        }
        System.out.println(c);


        System.out.println();
        System.out.println("Введи строку:");
        String str1=sc.next();
        int mid=str1.length()/2;
        if(str1.length()%2!=0){
            int i=mid-1;
            int j=mid+1;
            while(mid!=0){
                if(str1.charAt(i)!=str1.charAt(j)) {
                    System.out.println("NO");
                    return;
                }
                i--; j++; mid--;
            }
            System.out.println("YES");
        }
        else{
            mid--;
            int i=mid+1;
            while(mid!=-1){
                if(str1.charAt(mid)!=str1.charAt(i)){
                    System.out.println("NO");
                    return;
                }
                i++; mid--;
            }
            System.out.println("YES");
        }
    }
}
