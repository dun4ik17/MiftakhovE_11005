import java.util.Scanner;

public class DZ6 {
    public static int fac(int n, int m, int c){
        if(c<=n){
            m*=c; c++;
            return fac(n, m, c);
        }
        return m;
    }
    public static void tas2(int a, int b){
        if(a<=b){
            System.out.print(a +" ");
            a++;
            tas2(a, b);
        }
    }
    public static void fib(int n, int[] ar, int i){
        ar[i]=ar[i-1]+ar[i-2];
        i++;
        if(i<n) fib(n, ar, i);
    }
    public static int Akker(int m, int n){
        if(m==0) return n+1;
        else if(m>0 && n==0) return Akker(m-1, 1);
        else if(m>0 && n>0) return Akker(m-1, Akker(m, n-1));
        else return 0;
    }
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Введи число n для 1 задачи");
        int n=sc.nextInt();
        System.out.println("Факториал:");
        System.out.println(fac(n, 1, 2));
        System.out.println("Введи числа A и B для 2 задачи");
        double a=sc.nextDouble();
        double b=sc.nextDouble();
        int a1=0;
        int b1=(int) b;
        if(a%1==0) a1=(int)a;
        else a1=(int)a + 1;
        System.out.println("Целые числа:");
        tas2(a1, b1);
        System.out.println();
        System.out.println("Введи число n для Чисел Фибоначчи");
        n=sc.nextInt();
        int[] ar=new int[n];
        ar[0]=0;
        ar[1]=1;
        System.out.println("N-ое число Фибоначчи: ");
        fib(n, ar, 2);
        System.out.println(ar[n-1]);
        System.out.println("Введи числа m и n");
        int m=sc.nextInt();
        n=sc.nextInt();
        System.out.println("Результат функции Аккермана:");
        System.out.println(Akker(m,n));
    }
}