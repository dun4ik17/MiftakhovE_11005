import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    static boolean answerOfUser = true;
    static ArrayList<Order> orders = new ArrayList<>();
    static Scanner sc = new Scanner(System.in);
    static int id = 1;

    public static void addProducts(Order order){
        answerOfUser = true;

        while(answerOfUser){
            System.out.println("Enter data of product");
            System.out.println("(Name/Cost/Manufacturer) (20 symbols max)");

            sc.nextLine();
            String productName = sc.nextLine();
            double cost = sc.nextDouble();
            sc.nextLine();
            String manufacturer = sc.nextLine();

            Product product = new Product(productName, cost, manufacturer);
            order.setProducts(product);

            System.out.println("Do you want to add more products?");
            System.out.println("(Yes/No)");

            String answer = sc.next();
            while(!answer.equals("Yes") && !answer.equals("No")){
                System.out.println("Please, write right answer (Yes/No)");
                answer = sc.next();
            }
            if(answer.equals("No"))
                answerOfUser = false;
        }
    }
    public static void firstRequest(){
        System.out.println("Enter the personal data of customer, please");
        System.out.println("(Name/Sex/Age) (20 symbols max)");

        sc.nextLine();
        String customerName = sc.nextLine();
        String sex = sc.next();
        int age = sc.nextInt();

        Customer customer = new Customer(customerName, sex, age);
        Order order = new Order();
        order.setCustomer(customer);
        order.setId(id);

        System.out.println("Id of the order " + id);
        System.out.println("Make your order");

        addProducts(order);
        orders.add(order);
        id++;
    }

    public static void secondRequest(int id){
        Order order =  orders.get(id-1);
        addProducts(order);
    }

    public static void thirdRequest(){
        for (int i = 0; i < orders.size(); i++) {
            Order order = orders.get(i);
            Customer customer = order.getCustomer();
            System.out.println("========================================");
            System.out.println("Name: " + customer.getName());
            System.out.println("Sex:  " + customer.getSex());
            System.out.println("Age:  " + customer.getAge());
            System.out.println("----------------------------");

            ArrayList<Product> products = order.getProduct();
            System.out.println("Order " + order.getId());
            for (int j = 0; j < products.size(); j++) {
                System.out.println("Name:         " + products.get(j).getName());
                System.out.println("Cost:         " + products.get(j).getCost() + " RUB");
                System.out.println("Manufacturer: " + products.get(j).getManufacturer());
                if(j!= products.size()-1)
                    System.out.println("----------------------------");
            }
            System.out.println("========================================");

            System.out.println("Enter 'Y'");
            sc.next();
        }
    }

    public static void thirdRequest(int id){
        Order order = orders.get(id-1);
        Customer customer = order.getCustomer();
        System.out.println("========================================");
        System.out.println("Name: " + customer.getName());
        System.out.println("Sex:  " + customer.getSex());
        System.out.println("Age:  " + customer.getAge());
        System.out.println("----------------------------");

        ArrayList<Product> products = order.getProduct();
        System.out.println("Order " + order.getId());
        for (int j = 0; j < products.size(); j++) {
            System.out.println("Name:         " + products.get(j).getName());
            System.out.println("Cost:         " + products.get(j).getCost() + " RUB");
            System.out.println("Manufacturer: " + products.get(j).getManufacturer());
            if(j!= products.size()-1)
                System.out.println("----------------------------");
        }
        System.out.println("========================================");
    }

    public static void main(String[] args) {
        int userRequest = 1;
        System.out.println("Hello, now you see 3 function, ");
        System.out.println("that you can use at this program");
        System.out.println();

        while(userRequest > 0){
            System.out.println("-----------------------------------");
            System.out.println("1.Make an order");
            System.out.println("2.Add some products into some order");
            System.out.println("3.The data of order(s)");
            System.out.println("If you don't want to use the program,");
            System.out.println("just enter '0'");
            System.out.println("-----------------------------------");
            System.out.println();

            System.out.println("Choose the function and enter the number");
            System.out.println();

            userRequest = sc.nextInt();
            if(userRequest == 1)
                firstRequest();
            else if(userRequest == 2) {
                System.out.println("Enter id of the order");
                int id = sc.nextInt();
                secondRequest(id);
            }
            else if(userRequest == 0){
                System.out.println("Thank you for using our program :)");
                break;
            }
            else {
                System.out.println("Enter id of the order, otherwise get all data");
                int id = sc.nextInt();
                if(id > 0) 
                    thirdRequest(id-1);
                else 
                    thirdRequest();
            }
        }
    }
}