import java.util.ArrayList;

public class Order {
    private int id;
    private Customer customer;
    private ArrayList<Product> products = new ArrayList<>();

    public void setCustomer(Customer customer){this.customer = customer;}
    public void setProducts(Product product){products.add(product);}

    public Customer getCustomer(){return customer;}
    public ArrayList<Product> getProduct(){return products;}
    public int getId(){return id;}
    public void setId(int id){this.id = id;}

}
