import java.util.Scanner;

public class DZ3 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Введи количество строк и столбцов (n,m)");
        int n=sc.nextInt();
        int m=sc.nextInt();
        int[][] ar1=new int[n][m];
        int[][] ar2=new int[m][n];
        int[][] mat=new int[n][n];
        System.out.println();
        System.out.println("Введи значения 1 матрицы (n,m)");
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                ar1[i][j]=sc.nextInt();
            }
        }
        System.out.println();
        System.out.println("Введи значения 2 матрицы (m,n)");
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                ar2[i][j]=sc.nextInt();
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                for (int k = 0; k < m; k++) {
                    mat[i][j]+=ar1[i][k]*ar2[k][j];
                }
            }
        }
        System.out.println();
        System.out.println("Перемножение матриц:");
        for (int i = 0; i < n; i++) {
            System.out.println();
            for (int j = 0; j < n; j++) {
                System.out.print(mat[i][j]+" ");
            }
        }


        System.out.println();
        System.out.println("Введи строку для 2 задания");
        sc.nextLine();
        String str = sc.nextLine();
        int id = 0;
        int c = 0;
        int max = 0;
        String strmax = "";
        String str1 = "";
        while (id < str.length()) {
            while (str.charAt(id) == ' ') {
                id++;
                if (id == str.length()) break;
            }
            while (str.charAt(id) != ' ') {
                str1 += str.charAt(id);
                id++;
                c++;
                if (id == str.length()) {
                    if (c > max) {
                        max = c;
                        strmax = str1;
                    }
                    break;
                }
                if (str.charAt(id) == ' ') {
                    if (c > max) {
                        max = c;
                        strmax = str1;
                    }
                }
            }
            str1="";
            c=0;
        }
        System.out.println(strmax);


        System.out.println("Введи значения матрицы 2х2");
        int[][] matr1=new int[2][2];
        int[][] matr2=new int[3][3];
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                matr1[i][j]=sc.nextInt();
            }
        }
        System.out.println();
        System.out.println("Введи значения матрицы 3х3");
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                matr2[i][j]=sc.nextInt();
            }
        }
        System.out.println();
        System.out.println("Вычисление Определителя матрицы (2х2):");
        int opr1= matr1[0][0]*matr1[1][1]-matr1[1][0]*matr1[0][1];
        System.out.println(opr1);
        System.out.println("Вычисление Определителя матрицы (3х3):");
        int opr2=matr2[0][0]*(matr2[1][1]*matr2[2][2]-matr2[2][1]*matr2[1][2])-
                matr2[1][0]*(matr2[0][1]*matr2[2][2]-matr2[2][1]*matr2[0][2])+
                matr2[2][0]*(matr2[0][1]*matr2[1][2]-matr2[1][1]*matr2[0][2]);
        System.out.println(opr2);


        System.out.println("Введи количество строк и столбцов матрицы");
        n=sc.nextInt();
        m=sc.nextInt();
        int[][] arr=new int[n][m];
        System.out.println("Введи элементы матрицы");
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                arr[i][j]=sc.nextInt();
            }
        }
        for (int i = 0; i < m; i++){
            for (int j = i+1; j < m; j++) {
                if(arr[0][j]<arr[0][i]){
                    for (int k = 0; k < n; k++) {
                        int t=arr[k][i];
                        arr[k][i]=arr[k][j];
                        arr[k][j]=t;
                    }
                }
            }
        }
        System.out.println();
        System.out.println("Сортировка:");
        for (int i = 0; i < n; i++) {
            System.out.println();
            for (int j = 0; j < m; j++) {
                System.out.print(arr[i][j]+" ");
            }
        }
    }
}
