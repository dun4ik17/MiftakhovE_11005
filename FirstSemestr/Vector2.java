public class Vector2 {
    double x;
    double y;
    public void add(Vector2 other){
        this.x +=other.x;
        this.y +=other.y;
    }
    public  void sub(Vector2 other){
        this.x -=other.x;
        this.y -=other.y;
    }
    public void mult(double t){
        this.x *=t;
        this.y *=t;
    }
}