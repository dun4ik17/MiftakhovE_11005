import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        Random random = new Random();
        System.out.println("Enter amount of Hp");
        int hp = sc.nextInt();
        System.out.println("Enter name of Player 1");
        Player player1 = new Player(sc.next(), hp);
        System.out.println("Enter name of Player 2");
        Player player2 = new Player(sc.next(), hp);
        Game game = new Game();
        game.gameStart(player1, player2);
    }
}
