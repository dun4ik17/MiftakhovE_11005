import java.util.Random;
import java.util.Scanner;

public class Game {
    public static void statistics(Player player1, Player player2){
        System.out.println("Name: " + player1.getName() + "   " + player2.getName());
        System.out.println("Hp:   " + player1.getHp() + "      " + player2.getHp());
    }
    public static void turn(Player player){
        System.out.println("Turn: " + player.getName());
    }
    public static boolean probability(int power){
        Random random = new Random();
        if(power == 1) return true;
        else {
            int n = random.nextInt(12) + 1;
            if (n < 13 - power) return false;
            else return true;
        }
    }
    public void gameStart(Player player1, Player player2){
        statistics(player1, player2);
        Scanner sc=new Scanner(System.in);
        Random random = new Random();
        int turnPlayer = random.nextInt(1);
        while(player1.getHp()>0 || player2.getHp()>0){
            if(turnPlayer == 0)
                turn(player1);
            else
                turn(player2);
            System.out.println("Enter power from 1 to 9 (don't try set up more than 9)");
            int damage = sc.nextInt();
            if(damage > 9){
                System.out.println("BURN IN HELL, Fucking IDIOT");
                if(turnPlayer == 0)
                    player1.damage(player1.getHp());
                else
                    player2.damage(player2.getHp());
                break;
            }

            if(probability(damage)){
                System.out.println("Cool, you got it");
                if(turnPlayer == 0){
                    player2.damage(damage);
                }
                else {
                    player1.damage(damage);
                }
            }
            else System.out.println("Sorry dude, you didn't get it");
            if(turnPlayer == 0)
                turnPlayer = 1;
            else
                turnPlayer = 0;
            System.out.println();
            System.out.println("Press 'E' and Enter to continue");
            sc.next();
            statistics(player1, player2);
            if(player1.getHp()<=0 || player2.getHp()<=0) break;
        }
        String winner = new String();
        if(player1.getHp() <= 0)
            winner = player2.getName();
        else
            winner = player1.getName();
        System.out.println(winner + " WIN, My Congratulations =)");
        System.out.println("Do you want to play another one?");
        System.out.println("(Yes/No)");
        String answer = new String();
        answer = sc.next();
        if(answer.equals("Yes") || answer.equals("yes")) {
            System.out.println("Enter amount of Hp");
            int hp = sc.nextInt();
            System.out.println("Enter name of Player 1");
            player1.setName(sc.next());
            player1.setHp(hp);
            System.out.println("Enter name of Player 2");
            player2.setName(sc.next());
            player2.setHp(hp);
            gameStart(player1, player2);
        }
        else
            System.out.println("Thank you for the game, good bye");
    }
}
